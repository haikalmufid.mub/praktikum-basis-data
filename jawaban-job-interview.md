# NO 1
Tabel design aplikasi Shopee
![Tabel design](https://gitlab.com/haikalmufid.mub/praktikum-basis-data/-/raw/main/Tabel1.gif)

# NO 2
DDL aplikasi Shopee
![DDL](https://gitlab.com/haikalmufid.mub/praktikum-basis-data/-/raw/main/DDL.gif)

# NO 3
DML aplikasi Shopee
![DML](https://gitlab.com/haikalmufid.mub/praktikum-basis-data/-/raw/main/DML.gif)

# NO 4
DQL aplikasi Shopee
![DQL](https://gitlab.com/haikalmufid.mub/praktikum-basis-data/-/raw/main/DQL.gif)

# NO 5
[Video penjelasan dari DDL,DML dan DQL](https://youtu.be/umagJx8_4lA)
